package com.automation.walmart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage{

	/**
	 * Constructor
	 * @param driver: WebDriver
	 */
	public LoginPage(WebDriver driver) {
		super(driver);	
	}
	
	@FindBy(id="email")
	private WebElement userField;
	
	@FindBy(id="password")
	private WebElement passField;
	
	@FindBy(css="#sign-in-form > button.button.m-margin-top")
	private WebElement loginButton;
	
	/**
	 * Navigates to login Page.
	 */
	public PrivateHomePage Login(String user,  String pass) {
		
		this.write(userField, user);
		this.write(passField, pass);
		this.click(loginButton);
		return new PrivateHomePage(getDriver());
		
	}

	
}
