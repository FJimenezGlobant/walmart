package com.automation.walmart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class PrivateHomePage extends BasePage{

	
	/**
	 * Constructor
	 * @param driver: WebDriver
	 */
	public PrivateHomePage(WebDriver driver) {
		super(driver);		
	}
	
	@FindBy(css=".POVCarousel")
	private WebElement historyContainer;
	
	/**
	 * Validates that history container is displayed
	 */
	public boolean validateLogin() {
		
		getWait().until(ExpectedConditions.visibilityOf(historyContainer));
		return historyContainer.isDisplayed();
		
	}
	
}
