package com.automation.walmart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


/**
 * @author f.jimenez
 *
 */
public class PublicHomePage extends BasePage{
	
	/**
	 * Constructor
	 * @param driver: WebDriver
	 */
	public PublicHomePage(WebDriver driver, String url) {
		super(driver);
		driver.get(url);		
	}
	
	@FindBy(id="header-account-toggle")
	private WebElement accountButton;
	
	@FindBy(css="#signed-out > a:nth-child(1)")
	private WebElement signInButton;
	
	/**
	 * Log in to the private page.
	 */
	public LoginPage navigateToLogin() {
		
		this.click(accountButton);
		this.click(signInButton);
		return new LoginPage(getDriver());
		
	}
	
}
