package com.automation.walmart.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author f.jimenez
 *
 */
public class BasePage {

	private WebDriver driver;
	private WebDriverWait wait;
	public Logger log = Logger.getLogger(BasePage.class);
	
	/**
	 * Constructor
	 * @param pageDriver: WebDriver
	 */
	public BasePage (WebDriver pageDriver) {
		PageFactory.initElements(pageDriver, this);
		this.wait = new WebDriverWait(pageDriver, 40);
		this.driver = pageDriver;
	}
	
	/**
	 * @return WebDriver
	 */
	public WebDriver getDriver() {
		return this.driver;
	}
	
	/**
	 * @return WebDriverWait
	 */
	public WebDriverWait getWait() {
		return this.wait;
	}
	
	/**
	 * Defines actions to do when tests finish
	 */
	public void dispose() {
		if (this.driver != null) {
			this.driver.quit();
		}
	}
	
	/**
	 * Waits for an element and click on it
	 */
	public void click(WebElement element) {
		getWait().until(ExpectedConditions.visibilityOf(element));
		element.click();
	}
	
	/**
	 * Waits for an element and send keys to it
	 */
	public void write(WebElement element, String keys) {
		getWait().until(ExpectedConditions.visibilityOf(element));
		element.click();
		element.sendKeys(keys);;
	}
	
}
