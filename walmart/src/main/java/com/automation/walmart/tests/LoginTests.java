package com.automation.walmart.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.automation.walmart.pages.LoginPage;
import com.automation.walmart.pages.PrivateHomePage;
import com.automation.walmart.providers.CredentialsProvider;


/**
 * @author f.jimenez
 *
 */
public class LoginTests extends BaseTest{

	/**
	 * Test W002
	 * Verify if I can log in to the walmart page
	 */
	@Test(dataProvider = "Credentials", dataProviderClass = CredentialsProvider.class)
	public void LoginTest(String validUser, String validPass, String invalidPass) {
		
		log.info("--- Test of log in to the plattform ---");
		home = getHome();
		LoginPage login = home.navigateToLogin();
		PrivateHomePage privateHome= login.Login(validUser, validPass);
		Assert.assertTrue(privateHome.validateLogin(),"The login was not successfull");
		
		
	}	
		
}
