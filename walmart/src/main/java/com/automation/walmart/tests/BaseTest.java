package com.automation.walmart.tests;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.automation.walmart.Driver;
import com.automation.walmart.pages.PublicHomePage;
import com.automation.walmart.tests.BaseTest;

/**
 * @author f.jimenez
 *
 */
public class BaseTest {

	private Driver driver;
	public Logger log = Logger.getLogger(BaseTest.class);
	protected PublicHomePage home;
	
	@BeforeTest(alwaysRun=true)
	@Parameters({"browser", "url"})
	public void beforeSuite(String browser, String url) {
		this.driver = new Driver(browser);
		this.home = new PublicHomePage(this.driver.getDriver(), url);
		org.apache.log4j.BasicConfigurator.configure();
	}
	
	@AfterTest(alwaysRun=true)
	public void afterTest() {
		this.home.dispose();
	}
	
	public PublicHomePage getHome() {
		return this.home;
	}
}