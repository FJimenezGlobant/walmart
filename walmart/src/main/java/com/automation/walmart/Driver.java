package com.automation.walmart;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * @author f.jimenez
 *
 */
public class Driver {

	private WebDriver driver;
	
	/**
	 * Constructor
	 * @param browser: String
	 */
	public Driver (String browser) {
		
		switch (browser) {
		
		case "firefox":
			driver = (WebDriver) new FirefoxDriver();
			break;
			
		case "chrome":
			System.setProperty("webdriver.chrome.driver", "/Users/f.jimenez/Documents/drivers/chromedriver_win32/chromedriver.exe");
			driver = (WebDriver) new ChromeDriver();
			break;
			
		}
		
	}
	
	/**
	 * @return WebDriver
	 */
	public WebDriver getDriver() {
		return this.driver;
	}
}
