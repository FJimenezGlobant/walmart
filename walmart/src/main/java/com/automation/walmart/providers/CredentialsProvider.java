package com.automation.walmart.providers;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.testng.annotations.DataProvider;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author f.jimenez
 *
 */
public class CredentialsProvider {
	
	 /**
	   * Loads Login Test parameters.
	   * @return Object[][]
	   */
	  @DataProvider(name = "Credentials")
	  public static Object[][] parameters() {	  
	       return new Object[][] {{ getJsonDataProperty(VALIDUSER),
						        	getJsonDataProperty(VALIDPASS),						        	
						        	getJsonDataProperty(INVALIDPASS) }}; 
	  }
	  
	  private static final String VALIDUSER = "validUser";
	  private static final String VALIDPASS = "validPass";
	  private static final String INVALIDPASS = "invalidPass";
	  private static final String JSON_FILE_PATH = "src/main/java/com/automation/walmart/data.json";
	  private static JsonParser parser = new JsonParser();

		
		/**
		 * Method for get data form JSON file.
		 * 
		 * @author f.jimenez
		 * 
		 * @param property
		 *            : String
		 * @return String
		 */
		private static String getJsonDataProperty(String property) {
			try {
				Object obj = parser.parse(new FileReader(JSON_FILE_PATH));
				JsonObject jsonObject = (JsonObject) obj;
				return jsonObject.get(property)
					.getAsString();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "";
		}
}
